@echo off

echo Deleting contents of 'out' directory...
rmdir /s /q "out" 2>_ul
mkdir "out"

echo Copying index.html from 'src' to 'out'...
copy "src\index.html" "out\index.html"

echo Running TypeScript Compiler...
tsc

echo Script execution completed.
