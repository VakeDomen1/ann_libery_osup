import { Point, generatePoints } from "./data.js";
import { initGfx, setDrawables } from "./gfx.js";
import { Perceptron } from "./perceptron.js";

async function main(){
    initGfx();

    const points = generatePoints(500);
    console.table(points);
    setDrawables(points);

    const model = new Perceptron(2);

    for(const point of points){
        const inputVector = [point.x, point.y];
        const error = model.fitOne(inputVector, point.label);
        predAll(model, points);
        await sleep(100);
    }
}

function predAll (model: Perceptron, data: Point[]){
    for (const pt of data){
        const inputVector = [pt.x, pt.y];
        const prediction = model.predOne(inputVector);
        pt.guessed = (prediction == pt.label);
    }    
}

const sleep = (ms: number) => {
    return new Promise((resolve => setTimeout(resolve, ms)));
}

window.onload = main;